DIST = buster
ARCH = amd64
BUILDPLACE = build
APTCACHE = aptcache
HOOKDIR = hooks
BUILDRESULT = result

define sanity_checks
	if [ "$$(id -u)" != "0" ]; then \
		echo "superuser access required" >&2; \
		exit 1; \
	fi
	mkdir -p ${BUILDPLACE}
	mkdir -p ${APTCACHE}
	mkdir -p ${HOOKDIR}
	mkdir -p ${BUILDRESULT}
endef

define unpack
	echo "I: extracting base tarball [${DIST}.tgz]"
	tar -C ${BUILDPLACE} -x -f ${DIST}.tgz
	echo pbuilder >${BUILDPLACE}/etc/debian_chroot
endef

define pbuilder
	MAKEFLAGS= \
	MAKELEVEL= \
	MAKE_TERMERR= \
	MFLAGS= \
		pbuilder $@ \
		--distribution ${DIST} \
		--architecture ${ARCH} \
		--basetgz ${DIST}.tgz \
		--buildplace ${BUILDPLACE} \
		--aptcache ${APTCACHE} \
		--hookdir ${HOOKDIR} \
		--buildresult ${BUILDRESULT} \
		--extrapackages "fakeroot debian-keyring" \
		$(1) ${PBUILDER_OPTIONS}
endef

help:
	@echo "Usage: ${MAKE} help|create|clean|login|build|update [PBUILDER_OPTIONS=\"--foo\"]"

create clean update:
	@$(call sanity_checks)
	@$(call pbuilder)

login build:
	@$(call sanity_checks)
	@$(call unpack)
	@scripts/push.sh ${BUILDPLACE}
	@$(call pbuilder,--preserve-buildplace)

.PHONY: help create clean login build update
