#!/bin/sh
user=pbuilder
tz=Asia/Jakarta

useradd -m $user >/dev/null
passwd -d $user >/dev/null
ln -sf /usr/share/zoneinfo/$tz /etc/localtime

cat >/etc/profile.d/pbuilder.sh <<EOF
chown $user:$user /build

eval \`dircolors\`
alias ls='ls --color=auto'

export DEBEMAIL="zanculmarktum@gmail.com"
export DEBFULLNAME="Azure Zanculmarktum"

complete -F _quilt_completion -o filenames dquilt
EOF
chmod 755 /etc/profile.d/pbuilder.sh

cat >/usr/local/bin/dquilt <<'EOF'
quilt --quiltrc=${HOME}/.quiltrc-dpkg "$@"
EOF
chmod 755 /usr/local/bin/dquilt
cat <<'EOF' | tee /root/.quiltrc-dpkg /home/$user/.quiltrc-dpkg >/dev/null
d=. ; while [ ! -d $d/debian -a `readlink -e $d` != / ]; do d=$d/..; done
if [ -d $d/debian ] && [ -z $QUILT_PATCHES ]; then
  # if in Debian packaging tree with unset $QUILT_PATCHES
  QUILT_PATCHES="debian/patches"
  QUILT_PATCH_OPTS="--reject-format=unified"
  QUILT_DIFF_ARGS="-p ab --no-timestamps --no-index --color=auto"
  QUILT_REFRESH_ARGS="-p ab --no-timestamps --no-index"
  QUILT_COLORS="diff_hdr=1;32:diff_add=1;34:diff_rem=1;31:diff_hunk=1;33:diff_ctx=35:diff_cctx=33"
  if ! [ -d $d/debian/patches ]; then mkdir $d/debian/patches; fi
fi
EOF
