#!/bin/sh
#
# Push files into $BUILDPLACE.

BUILDPLACE="$1"; shift

mkdir -p $BUILDPLACE/var/lib/apt/lists
cp -lrf ../lists/* $BUILDPLACE/var/lib/apt/lists/

rsync -lprt \
  --exclude=sources.list.d/sid.list \
  --exclude=sources.list.d/testing.list \
  --exclude=sources.list.d/php.list \
  --exclude=sources.list.d/nodesource.list \
  --exclude=sources.list.d/megasync.list \
  --exclude=sources.list.d/nginx.list \
  --exclude=sources.list.d/heroku.list \
  /etc/apt/sources.list \
  /etc/apt/sources.list.d \
  /etc/apt/trusted.gpg.d \
  $BUILDPLACE/etc/apt
